## Usage

Install dependencies
```
$ npm install
```
or
```
yarn
```

For development
```bash
$ npm run dev
```

Build web app scripts and styles:
```bash
$ npm run build
```

For production
```bash
$ npm start
```

## Docker

Building the images for the first time
```
$ docker-compose build
```

Starting the images
```
$ docker-compose up
```
